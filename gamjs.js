/**
 * @author Said El Mazghari
 */

/**Variabelen**/
var coordinate = new Array(30, 0);
var namen = new Array("obstacle1", "obstacle2", "obstacle3", "obstacle4");
var coordinateobst = [[200, 300], [200, 100], [50, 200], [50, 200]]; /* (verticaal , horizontaal)*/
var money = new Array("money1", "money2", "money3", "money4");
var coordinatemoney = [[200, 300], [200, 100], [50, 200], [50, 200]];
var newgame = false;
var positionback = 0;
var jump = false;
var bckgrndspeed = 2;
var vampirespeed = 0.001;
var hoogte = 100;
var counter = 1;
var down = false;
var on = true;
var change = false;
var tekenen;
var tekenen2;
var resultaat;
var transition;
var naam = "";
var score = 0;
var highscore = 0;
var jmpsound = new Audio("ressource/sound/jump.ogg");
var ddsound = new Audio("ressource/sound/dead.ogg");
var bcksound = new Audio("ressource/sound/backmusic.mp3");
var gameoversound = new Audio("ressource/sound/gameover.mp3");
/**var coins  = new Audio("ressource/sound/coins.mp3");**/
var coins = new Audio("ressource/sound/slurp.mp3");

bcksound.volume = 0.5;
/** START FUNCTIES**/
$(document).ready(function () {
            $("#start").fadeIn(0);
            $("#startmenu").fadeOut(0);
        });

        (function () {
            document.getElementById('start').style.bottom = document.getElementById('naam').style.bottom = document.getElementById('Menupauze').style.bottom = (window.innerHeight / 2) - 100 + 'px';
            document.getElementById('start').style.left = document.getElementById('naam').style.left = document.getElementById('Menupauze').style.left = (window.innerWidth / 2) - 250 + 'px';
            bcksound.play();
            for (var i = 0; i < 4; i++) {
                coordinateobst[i][0] = Math.floor((Math.random() * 500) + window.innerWidth);
                coordinateobst[i][1] = Math.floor((Math.random() * (window.innerHeight - 60)) + 1);
                document.getElementById(namen[i]).style.left = coordinateobst[i][0] + 'px';
                document.getElementById(namen[i]).style.bottom = coordinateobst[i][1] + 'px';


            };
            for (var i = 0; i < 4; i++) {
                coordinatemoney[i][0] = Math.floor((Math.random() * 200) + window.innerWidth + 500);
                coordinatemoney[i][1] = Math.floor((Math.random() * (window.innerHeight - 60)) + 1);
                document.getElementById(namen[i]).style.left = coordinatemoney[i][0] + 'px';
                document.getElementById(namen[i]).style.bottom = coordinatemoney[i][1] + 'px';


            };


        })();
        /**Getfuncties**/
        function getCoordx() {

            return coordinate[0];
        }

        function getCoordy() {

            return coordinate[1];
        }

        function displaywithopacity(idname) {

            document.getElementById(idname).style.display = "block";

            document.getElementById(idname).style.opacity = 1;

        }

        function displaywithopacity2(idname) {
            document.getElementById(idname).style.display = "block";
            var oppac = 0;
            transition = window.setInterval(function () {
                oppac += 0.1;
                if (oppac <= 1)
                    document.getElementById(idname).style.opacity = oppac;
                if (oppac > 1) {
                    change = true;
                    clearInterval(transition);
                }
            }, 20);

        }

        function nodisplay(idname) {
            document.getElementById(idname).style.display = "none";
            document.getElementById(idname).style.opacity = 0;
        }

        function nodisplaywithopacity() {
            var oppac = 1;
            transition = window.setInterval(function () {
                oppac -= 0.1;
                if (oppac >= 0)
                    document.getElementById("levelup").style.opacity = oppac;
                if (oppac < 0) {
                    document.getElementById("levelup").style.opacity = 0;
                    clearInterval(transition);
                }
            }, 10);


        }
        /** BUTTONS**/
        function buttonsubmit() {

            if (document.getElementById('naaminvoer').value == "")
                alert("Voer uw naam in a.u.b.");
            else {
                newgame = true;
                nodisplay('naam');
                startteller();
                naam = document.getElementById('naaminvoer').value;
                displaywithopacity('vmpimg');
                displaywithopacity('pauzebouton');
                displaywithopacity('score2');
                displaywithopacity('top');
                for (var i = 0; i < 4; i++) {

                    document.getElementById(namen[i]).style.display = "block";
                };
                for (var i = 0; i < 4; i++) {

                    document.getElementById(money[i]).style.display = "block";
                };
                tekenman();
            }
        }

        function start() {
            document.getElementById('start').style.display = "none";
            displaywithopacity2('naam');

        }

        function startteller() {


            tekenen = window.setInterval(function () {
                positionback--;
                document.body.style.backgroundPositionX = positionback + 'px';
                collisiondetect();
                collitioncoin();
                levelup();
            }, bckgrndspeed);
            tekenen2 = window.setInterval(function () {
                tekenman();
            }, vampirespeed);
            resultaat = window.setInterval(function () {
                stappen();
                obstacle();
                coin();
                counter++;
                document.getElementById("score2").value = "Score : " +
                    score++ + '         ' + 'highscore : ' + highscore;
                if (highscore < score) highscore = score, 1000
            })
        }

        function stappen() {
            if (!jump && !down) {
                if (score % 5 == 0)
                    document.getElementById("vmpimg").src = "ressource/vampire5.png";
                else
                    document.getElementById("vmpimg").src = "ressource/vampire6.png";
            }
        }

        function pauze() {
            stop();
            displaywithopacity('Menupauze');
            document.getElementById('score').value = naam + ' : ' + (score - 1);


        }

        function stop() {
            clearInterval(tekenen);
            clearInterval(tekenen2);
            clearInterval(resultaat);
            nodisplay('vmpimg');
            nodisplay('pauzebouton');
            nodisplay('score2');
            nodisplay('top');
            for (var i = 0; i < 4; i++) {
                nodisplay(namen[i]);
            };
        }

        function resume() {
            nodisplay('Menupauze');

            displaywithopacity('vmpimg');
            for (var i = 0; i < 4; i++) {
                displaywithopacity(namen[i]);
            };
            startteller();
        }

        function exit() {
            nodisplay('Menupauze');
            displaywithopacity('start');


            score = 0;
            coordinate[0] = 30;
            coordinate[1] = 1;
        }

        function collisiondetect() {




            var widthvamp = 50;

            var heightvamp = 60;

            /** top left**/

            for (var i = 0; i < 4; i++) {
                var x1 = coordinateobst[i][0] + 10;
                var y1 = coordinateobst[i][1];
                var x = coordinate[0];
                var y = coordinate[1] + 10;
                var draw = 0;
                if ((((x + widthvamp) > x1) && ((x + widthvamp) < (x1 + 90))) || (((x > x1) && (x < (x1 + 90))))) {
                    if ((((y + heightvamp) < (y1 + 90)) && ((y + heightvamp) > y1)) || ((y > y1) && (y < (y1 + 90)))) {
                        newgame = false;
                        clearInterval(tekenen);
                        clearInterval(tekenen2);
                        clearInterval(resultaat);
                        nodisplay('score2');
                        nodisplay('top');
                        nodisplay('pauzebouton');
                        for (var i = 0; i < 4; i++) {
                            nodisplay(money[i]);
                        };
                        document.body.style.backgroundImage = 'url("ressource/background5.png")';
                        document.getElementById("vmpimg").src = "ressource/faseini.png";
                        dead = window.setInterval(function () {

                            if (coordinate[1] !== 0) {
                                coordinate[1] -= 5;

                                document.getElementById('vampire').style.bottom = getCoordy() + 'px';
                            }

                            if (draw <= 8) {
                                switch (draw) {
                                    case 0:
                                        document.getElementById("vmpimg").src = "ressource/faseini.png";
                                        draw++;
                                        ddsound.play();
                                        bcksound.volume = 0.4;
                                        break;
                                    case 1:
                                        document.getElementById("vmpimg").src = "ressource/fase0.png";
                                        draw++;
                                        break;
                                    case 2:
                                        document.getElementById("vmpimg").src = "ressource/fase1.png";
                                        draw++;
                                        break;
                                    case 3:
                                        document.getElementById("vmpimg").src = "ressource/fase2.png";
                                        draw++;
                                        bcksound.volume = 0.3;
                                        break;
                                    case 4:
                                        document.getElementById("vmpimg").src = "ressource/fase3.png";
                                        draw++;
                                        break;
                                    case 5:
                                        document.getElementById("vmpimg").src = "ressource/fase4.png";
                                        draw++;
                                        break;
                                    case 6:
                                        document.getElementById("vmpimg").src = "ressource/fase5.png";
                                        draw++;
                                        bcksound.volume = 0.2;
                                        break;
                                    case 7:
                                        document.getElementById("vmpimg").src = "ressource/fase6.png";
                                        draw++;
                                        break;
                                    case 8:
                                        document.getElementById("vmpimg").src = "ressource/fase7.png";
                                        draw++;
                                        bcksound.volume = 0.1;
                                        break;

                                }

                            }
                            if (draw == 9 && coordinate[1] <= 0) {
                                document.getElementById('vampire').style.bottom = 0 + 'px';
                                gameoversound.play();
                                displaywithopacity2("gameover");
                                displaywithopacity2("gamover");
                                nodisplay('vmpimg');
                                clearInterval(dead);
                                bcksound.volume = 0.05;
                                /*bcksound.pause();*/
                                for (var i = 0; i < 4; i++) {
                                    nodisplay(namen[i]);
                                };

                            }

                        }, 50);

                    };
                }
            }

        };


        function collitioncoin() {
            var widthvamp = 50;

            var heightvamp = 60;
            for (var i = 0; i < 4; i++) {
                var x1 = coordinatemoney[i][0];
                var y1 = coordinatemoney[i][1];
                var x = coordinate[0];
                var y = coordinate[1] + 10;
                if ((((x + widthvamp) >= x1) && ((x + widthvamp) <= (x1 + 24))) || (((x >= x1) && (x <= (x1 + 24))))) {
                    if ((((y + heightvamp) <= (y1 + 24)) && ((y + heightvamp) >= y1)) || ((y >= y1) && (y <= (y1 + 24)))) {
                        coordinatemoney[i][1] = -50;
                        score += 100;
                        coins.pause();
                        coins.currentTime = 0;
                        coins.play();


                    }
                }
            }
        }

        function obstacle() {
            for (var i = 0; i < 4; i++) {
                if (coordinateobst[i][0] < -500) {
                    coordinateobst[i][0] = Math.floor((Math.random() * 500) + window.innerWidth);
                    coordinateobst[i][1] = Math.floor((Math.random() * (window.innerHeight - 95)) + 1);


                } else {
                    coordinateobst[i][0] -= 2;
                }

                document.getElementById(namen[i]).style.left = coordinateobst[i][0] + 'px';
                document.getElementById(namen[i]).style.bottom = coordinateobst[i][1] + 'px';

            };

        }

        function coin() {

            for (var i = 0; i < 4; i++) {
                if (coordinatemoney[i][0] < -500) {
                    coordinatemoney[i][0] = Math.floor((Math.random() * 200) + window.innerWidth + 200);
                    coordinatemoney[i][1] = Math.floor((Math.random() * (window.innerHeight - 95)) + 1);

                } else {
                    coordinatemoney[i][0] -= 2;
                }

                document.getElementById(money[i]).style.left = coordinatemoney[i][0] + 'px';
                document.getElementById(money[i]).style.bottom = coordinatemoney[i][1] + 'px';

            };
        }

        function tekenman() {

            if (jump && coordinate[1] <= hoogte && coordinate[1] < window.innerHeight - 75)
                ++
                coordinate[1];
            else
                jump = false;

            /*if (coordinate[1] == (hoogte + 1) && !jump)
            	hoogte -= 50;*/

            if (!jump && coordinate[1] >= 1)
                coordinate[1]--;
            if (coordinate[1] == 0 && !down)
                document.getElementById("vmpimg").src = "ressource/vampire5.png";
            /*if (coordinate[1] == hoogte )
             jump = false;
             if (coordinate[1] >= 1 && !jump)
             coordinate[1]--;
             hoogte=100;*/

            document.getElementById('vampire').style.bottom = getCoordy() + 'px';
            document.getElementById('vampire').style.left = getCoordx() + 'px';

        }

        function levelup() {
            if (counter % 1000 == 0) {
                nodisplaywithopacity2();
                /*displaywithopacity2("levelup");*/

            }





        }
        document.onkeydown = function () {
            if (newgame) {
                switch (window.event.keyCode) {
                    case 38:
                        jump = true;
                        hoogte = coordinate[1] + 200;
                        coordinate[1] += 10;
                        if ((hoogte - coordinate[1]) < 10)
                            hoogte += 200;
                        document.getElementById("vmpimg").src = "ressource/vampirefly.png";
                        down = false;
                        jmpsound.play();
                        break;
                    case 39:
                        coordinate[0] += 5;
                        break;
                    case 40:
                        down = true;
                        document.getElementById("vmpimg").src = "ressource/vampirelay.png";
                        if (coordinate[1] >= 20)

                            coordinate[1] -= 20;
                        jump = false;

                        break;
                    case 37:

                        if (coordinate[0] >= 20)

                            coordinate[0] -= 20;
                        break;

                }
            }
        }
        /**-------------------------------------------------------------**/


        function infinitmusic() {
            if (bcksound.currentTime == 148)
                bcksound.currentTime == 10;

        }

        function musiconoff() {
            if (on) {

                bcksound.pause();
                document.getElementById("music").style.backgroundImage = 'url("ressource/soundoff.png")';
                on = false;
            } else {
                bcksound.play();
                bcksound.currentTime = 0;
                document.getElementById("music").style.backgroundImage = 'url("ressource/son.png")';
                on = true;
            }
        }